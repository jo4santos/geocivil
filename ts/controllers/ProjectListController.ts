"use strict"

module civilapp.controllers {
    export class ProjectListController {

        private $ionicActionSheet:Ionic.IActionSheet;
        private $ionicPopup:Ionic.IPopup;
        private $scope:IProjectListScope;
        private $state:ng.ui.IStateService;

        private ProjectService:services.ProjectService;

        public projects:any;

        public constructor($scope:IProjectListScope, ProjectService:services.ProjectService, $ionicActionSheet:Ionic.IActionSheet, $ionicPopup:Ionic.IPopup, $state:ng.ui.IStateService) {

            this.$ionicActionSheet = $ionicActionSheet;
            this.$ionicPopup = $ionicPopup;
            this.$scope = $scope;
            this.$state = $state;

            this.ProjectService = ProjectService;

            this.projects = [];

            this.load();

            console.debug(civilapp.utils.LogUtils.format("ProjectListController initialized"));
        }

        private load() {
            this.ProjectService.getList()
                .then((projects:model.Project[])=> {
                    this.projects = angular.copy(projects);
                })
        }

        public create() {
            this.$ionicPopup.prompt({
                title: "Insira um nome para o projeto",
                inputPlaceholder: "nome",
                inputType: 'text'
            }).then((projectLabel) => {
                if (projectLabel) {
                    this.ProjectService
                        .create(projectLabel)
                        .then((projectId:string)=> {
                            this.load();
                            this.$state.go("project", {projectId: projectId});
                        })
                        .catch(()=> {
                            alert("Erro ao criar");
                        })
                }
            });

        }


        public select(project:civilapp.model.Project) {

            var options:Ionic.IActionSheetOptions = {};

            options.titleText = project.label;
            options.buttons = [
                {text: "Abrir"},
                {text: "Partilhar"}
            ];
            options.buttonClicked = (index?:number) => {
                switch (index) {
                    case 0:
                        this.$state.go("project",{projectId:project.id})
                        break;
                    case 1:
                        this.share(project)
                        break;
                }
            }

            options.destructiveText = "Apagar";
            options.destructiveButtonClicked = () => {
                this.$ionicPopup.confirm({
                    title: 'Confirmar eliminação',
                    template: 'O projeto será removido e não poderá voltar atrás'
                }).then((res) => {
                    if(res) {
                        this.ProjectService.remove(project.id)
                            .then(() => {
                                this.load();
                                hideSheet();
                            })
                    }
                });
                return true;
            }

            options.cancelText = "Cancelar";
            options.cancel = () => {
            }


            var hideSheet:any = this.$ionicActionSheet.show(options);
        }

        public share(project:civilapp.model.Project) {
            alert("share " + project.id);
        }
    }

    interface IProjectListScope extends ng.IScope {
    }
}