"use strict"

module civilapp.controllers {
    export class PointController {

        private $cordovaGeolocation: any;
        private $ionicLoading: Ionic.ILoading;
        private $scope:IPointScope;
        private $state:ng.ui.IStateService;
        private $stateParams:IPointStateParams;

        private ElevationService:services.ElevationService;
        private ProjectService:services.ProjectService;

        public project:model.Project;
        public point:model.Point;

        public title:string;

        public constructor($scope:IPointScope, $state:ng.ui.IStateService, $stateParams:IPointStateParams, ElevationService:services.ElevationService, ProjectService:services.ProjectService, $cordovaGeolocation: any, $ionicLoading: Ionic.ILoading) {
            this.$cordovaGeolocation = $cordovaGeolocation;
            this.$ionicLoading = $ionicLoading;
            this.$scope = $scope;
            this.$state = $state;
            this.$stateParams = $stateParams;

            this.project = new model.Project();
            this.point = new model.Point();
            this.project.id = parseInt(this.$stateParams.projectId);
            this.point.id = parseInt(this.$stateParams.pointId);

            this.ElevationService = ElevationService;
            this.ProjectService = ProjectService;


            if (!this.point.id) {
                this.title = " - Novo ponto";
            }

            this.load();

            console.debug(civilapp.utils.LogUtils.format("PointController initialized"));
        }

        private load() {
            this.ProjectService.getById(this.project.id)
                .then((project:model.Project)=> {
                    this.project = angular.copy(project);

                    for (var i in this.project.points) {
                        if (this.project.points[i].id == this.point.id) {
                            this.point = angular.copy(this.project.points[i]);
                            break;
                        }
                    }

                    if (!this.point.id) {
                        this.point.id = 0;
                        for (var i in this.project.points) {
                            if (this.project.points[i].id > this.point.id) {
                                this.point.id = this.project.points[i].id;
                            }
                        }
                        this.point.id++;
                        if (!this.point.label || this.point.label == "") {
                            this.point.label = "Ponto " + this.point.id;
                        }
                        this.project.points.push(this.point);
                    }
                    this.title = " - " + this.point.label;
                })
        }

        public save() {
            if (!this.point.id) {
            }
            else {
                if (!this.point.label || this.point.label == "") {
                    this.point.label = "Ponto " + this.point.id;
                }
                for (var i in this.project.points) {
                    if (this.project.points[i].id == this.point.id) {
                        this.project.points[i] = angular.copy(this.point);
                        break;
                    }
                }
            }
            this.ProjectService.update(this.project);
        }

        public register() {
            this.point.measure = this.point.measure || new model.Measure();
            this.point.measure.altitude = null;

            this.$ionicLoading.show({template: 'Loading...'});

            this.$cordovaGeolocation
                .getCurrentPosition({timeout: 10000, enableHighAccuracy: true})
                .then((position) => {
                    this.point.measure.latitude = position.coords.latitude;
                    this.point.measure.longitude = position.coords.longitude;
                    this.$ionicLoading.hide();
                    this.save();
                })
                .catch((err) => {
                    console.log(err);
                    alert("ERROR");
                    this.$ionicLoading.hide();
                    // error
                });

            this.save();
        }

        public enhance() {
            this.point.measure = this.point.measure || new model.Measure();

            this.ElevationService.get(this.point.measure.latitude,this.point.measure.longitude)
            .then((data:any)=>{
                    this.point.measure.altitude = angular.copy(data);
                    this.save();
                });
        }
    }

    interface IPointScope extends ng.IScope {
    }

    interface IPointStateParams extends ng.ui.IStateParams {
        projectId: string;
        pointId: string;
    }
}