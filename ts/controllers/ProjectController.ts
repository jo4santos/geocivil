"use strict"

declare var cordova: any;

module civilapp.controllers {
    export class ProjectController {

        private $cordovaSocialSharing:any;
        private $cordovaFile:any;
        private $cordovaToast: any;
        private $ionicActionSheet:Ionic.IActionSheet;
        private $ionicPopup:Ionic.IPopup;
        private $scope:IProjectScope;
        private $state:ng.ui.IStateService;
        private $stateParams:IProjectStateParams;

        private ElevationService:services.ElevationService;
        private ExportService:services.ExportService;
        private ProjectService:services.ProjectService;

        public project:model.Project;

        public constructor($scope:IProjectScope, $ionicActionSheet:Ionic.IActionSheet, $ionicPopup:Ionic.IPopup, $state:ng.ui.IStateService, $stateParams:IProjectStateParams, ProjectService:services.ProjectService, ElevationService:services.ElevationService, ExportService:services.ExportService) {
            this.$ionicActionSheet = $ionicActionSheet;
            this.$ionicPopup = $ionicPopup;
            this.$scope = $scope;
            this.$state = $state;
            this.$stateParams = $stateParams;

            this.ElevationService = ElevationService;
            this.ExportService = ExportService;
            this.ProjectService = ProjectService;

            this.load();

            console.debug(civilapp.utils.LogUtils.format("ProjectController initialized"));
        }

        private load() {
            this.ProjectService.getById(parseInt(this.$stateParams.projectId))
                .then((project:model.Project)=> {
                    this.project = angular.copy(project);
                })
        }

        public select(point:civilapp.model.Point) {

            var options:Ionic.IActionSheetOptions = {};

            options.titleText = point.label;
            options.buttons = [
                {text: "Abrir"},
                {text: "Obter altitude"}
            ];
            options.buttonClicked = (index?:number) => {
                switch (index) {
                    case 0:
                        this.$state.go("point", {projectId: this.project.id, pointId: point.id});
                        hideSheet();
                        break;
                    case 1:
                        this.enhance(point);
                        hideSheet();
                        break;
                }
            }

            options.destructiveText = "Apagar";
            options.destructiveButtonClicked = () => {
                this.$ionicPopup.confirm({
                    title: 'Confirmar eliminação',
                    template: 'O ponto será removido e não poderá voltar atrás'
                }).then((res) => {
                    if (res) {
                        for (var i in this.project.points) {
                            if (this.project.points[i].id == point.id) {
                                this.project.points.splice(i, 1);
                                break;
                            }
                        }
                        this.ProjectService.update(this.project)
                            .then(() => {
                                this.load();
                                hideSheet();
                            })
                    }
                });
                return true;
            }

            options.cancelText = "Cancelar";
            options.cancel = () => {
            }


            var hideSheet:any = this.$ionicActionSheet.show(options);
        }

        public share(project:civilapp.model.Project) {



            this.ExportService.send(project);

        }

        public enhance(point:civilapp.model.Point) {


            point.measure = point.measure || new model.Measure();

            this.ElevationService.get(point.measure.latitude, point.measure.longitude)
                .then((data:any)=> {
                    point.measure.altitude = angular.copy(data);
                    this.save();
                });
        }

        public save() {
            this.ProjectService.update(this.project)
                .then(() => {
                    this.load();
                })
        }

        public remove () {
            this.$ionicPopup.confirm({
                title: 'Confirmar eliminação',
                template: 'O projeto será removido e não poderá voltar atrás'
            }).then((res) => {
                if (res) {
                    this.ProjectService.remove(this.project.id)
                        .then(() => {
                            this.$state.go("project-list")
                        })
                }
            });
            return true;
        }

        public getMapUrl() {
            var mapUrl:string = "https://maps.googleapis.com/maps/api/staticmap?size=200x200&";
            for (var i in this.project.points) {
                mapUrl += "markers=color:red|label:" + this.project.points[i].id + "|" + this.project.points[i].measure.latitude + "," + this.project.points[i].measure.longitude + "&";
            }
            mapUrl += "scale=2&sensor=true";
            return mapUrl;
        }
    }

    interface IProjectScope extends ng.IScope {
    }

    interface IProjectStateParams extends ng.ui.IStateParams {
        projectId: string;
    }
}