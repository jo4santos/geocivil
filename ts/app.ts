/// <reference path="utils/LogUtils.ts"/>

module civilapp {

    export class civilApp {

        public static ANGULAR_APP_NAME:string = "civilapp";

        private angularApp:ng.IModule;

        public constructor() {
            this.bootstrapAngular();
        }

        private bootstrapAngular() {

            this.angularApp = angular.module(civilApp.ANGULAR_APP_NAME, [
                'ionic',
                'ngCordova'
            ]);

            this.angularApp.provider("DatabaseService", function () {
                var DatabaseService;
                this.$get = ($q:ng.IQService) => {
                    if (DatabaseService == null) {
                        DatabaseService = new services.DatabaseService($q);
                    }
                    return DatabaseService;
                };
            });

            this.angularApp.provider("DatasourceFactory", function () {
                var DatasourceFactory;
                this.$get = (DatabaseService:civilapp.services.DatabaseService, $q:ng.IQService) => {
                    if (DatasourceFactory == null) {
                        DatasourceFactory = new datasources.DatasourceFactory($q, DatabaseService);
                    }
                    return DatasourceFactory;
                };
            });

            this.angularApp.provider("ProjectService", function () {
                var ProjectService;
                this.$get = ($q:ng.IQService, DatasourceFactory:civilapp.datasources.DatasourceFactory) => {
                    if (ProjectService == null) {
                        ProjectService = new services.ProjectService($q, DatasourceFactory);
                    }
                    return ProjectService;
                };
            });

            this.angularApp.provider("ElevationService", function () {
                var ElevationService;
                this.$get = ($q:ng.IQService, $http:ng.IHttpService) => {
                    if (ElevationService == null) {
                        ElevationService = new services.ElevationService($q, $http);
                    }
                    return ElevationService;
                };
            });

            this.angularApp.provider("ConversionService", function () {
                var ConversionService;
                this.$get = ($q:ng.IQService) => {
                    if (ConversionService == null) {
                        ConversionService = new services.ConversionService($q);
                    }
                    return ConversionService;
                };
            });

            this.angularApp.provider("ExportService", function () {
                var ExportService;
                this.$get = ($q:ng.IQService,$cordovaFile: ng.cordova.IFileService,$cordovaSocialSharing: ng.cordova.ISocialSharingService,$cordovaToast: ng.cordova.IToastService, ConversionService: services.ConversionService) => {
                    if (ExportService == null) {
                        ExportService = new services.ExportService($q,$cordovaFile,$cordovaSocialSharing,$cordovaToast, ConversionService);
                    }
                    return ExportService;
                };
            });

            this.angularApp.config(function ($urlRouterProvider:ng.ui.IUrlRouterProvider, $stateProvider:ng.ui.IStateProvider, $controllerProvider:ng.IControllerProvider) {
                $stateProvider
                    .state('init', {
                        url: '/init',
                        cache: false,
                        templateUrl: 'templates/init.html'
                    }).state('project-list', {
                        url: '/',
                        cache: false,
                        templateUrl: 'templates/project-list.html',
                        controller: 'ProjectListController as ProjectListController'
                    }).state('project', {
                        url: '/project/:projectId',
                        cache: false,
                        templateUrl: 'templates/project.html',
                        controller: 'ProjectController as ProjectController'
                    }).state('point', {
                        url: '/point/:projectId/:pointId',
                        cache: false,
                        templateUrl: 'templates/point.html',
                        controller: 'PointController as PointController'
                    });
                $urlRouterProvider.otherwise("/init");

                $controllerProvider.register("ProjectListController", controllers.ProjectListController);
                $controllerProvider.register("ProjectController", controllers.ProjectController);
                $controllerProvider.register("PointController", controllers.PointController);

            });

            this.angularApp.run(($rootScope:IAppRootScope, $state:ng.ui.IStateService, DatabaseService:civilapp.services.DatabaseService, $ionicHistory:any, $ionicPlatform:any) => {

                $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {

                    if (!DatabaseService.started && toState.name != "init") {
                        event.preventDefault();
                        $state.go("init");
                    }
                    else if (toState.name == "init") {
                        DatabaseService.start().then(()=> {
                            $ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            $state.go("project-list")
                        });
                    }
                });

            });
        }
    }

    export interface IAppRootScope extends ng.IRootScopeService {
        previousState: any;
        previousStateParams: any;
    }
}

new civilapp.civilApp();
console.debug("[" + new Date().toUTCString() + "] - civilApp has just started!");