module civilapp.datasources {

    export class ProjectDatasource {

        public static TABLE_NAME: string = "Project";

        private db: IDBDatabase;

        private $q: ng.IQService;

        constructor(DatabaseService: civilapp.services.DatabaseService,
                    $q: ng.IQService) {
            this.$q = $q;

            console.log("ProjectDatasource.constructor()");

            this.db = DatabaseService.get(civilapp.services.DatabaseService.APP_DATABASE_NAME);
        }

        create(project: civilapp.model.Project): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);

            var request: IDBRequest = store.add(project);

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                dfrd.resolve();
            };

            return dfrd.promise;
        }

        retrieveAll(): ng.IPromise<civilapp.model.Project[]> {
            var dfrd: ng.IDeferred<civilapp.model.Project[]> = this.$q.defer<civilapp.model.Project[]>();

            var projects: civilapp.model.Project[] = [];

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readonly");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);
            var request: IDBRequest = store.openCursor();

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                var result: IDBCursorWithValue = e.target.result;

                if(result) {
                    projects.push(result.value);
                    result.continue();
                }
                else {
                    dfrd.resolve(projects);
                }
            };

            return dfrd.promise;
        }



        retrieveById(id: string): ng.IPromise<civilapp.model.Project> {
            var dfrd: ng.IDeferred<civilapp.model.Project> = this.$q.defer<civilapp.model.Project>();

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readonly");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);
            var request: IDBRequest = store.get(id);

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                var result: Object = e.target.result;
                if (result != null) {
                    var project: civilapp.model.Project = new civilapp.model.Project();
                    angular.copy(result, project);
                    dfrd.resolve(project);
                } else {
                    dfrd.resolve(null);
                }
            };

            return dfrd.promise;
        }

        update(project: civilapp.model.Project): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);
            var request: IDBRequest = store.put(project);

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                dfrd.resolve();
            };

            return dfrd.promise;
        }

        removeById(id: number): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);
            var request: IDBRequest = store.delete(id);

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                dfrd.resolve();
            };

            return dfrd.promise;
        }

        clear(): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var transaction: IDBTransaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
            var store: IDBObjectStore = transaction.objectStore(ProjectDatasource.TABLE_NAME);
            var request: IDBRequest = store.clear();

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                dfrd.resolve();
            };

            return dfrd.promise;
        }

    }

}