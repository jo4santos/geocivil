module civilapp.datasources {

    export class DatasourceFactory {

        private DatabaseService: civilapp.services.DatabaseService;
        private $q: ng.IQService;

        constructor($q: ng.IQService,
                    DatabaseService: civilapp.services.DatabaseService) {

            this.DatabaseService = DatabaseService;

            console.debug("DatasourceFactory.constructor()");

            this.$q = $q;
        }

        public getProjectDatasource(): ProjectDatasource {
            return new ProjectDatasource(this.DatabaseService, this.$q);
        }
    }

}