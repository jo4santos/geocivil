module civilapp.services {

    export interface ObjectStoreIndex {
        name: string;
        keyPath: string;
        unique: boolean
    }

    export interface ObjectStoreSchema {
        name: string;
        keyPath: string;
        indexes: ObjectStoreIndex[];
    }

    export class DatabaseService {

        public static APP_DATABASE_VERSION: number = 1;
        public static APP_DATABASE_NAME: string = "AppDatabase";
        public static APP_DATABASE_SCHEMA: ObjectStoreSchema[] = [
            {
                name: "Project",
                keyPath: "id",
                indexes: []
            }
        ];

        private databases: {[name: string]: IDBDatabase};
        public started: boolean;

        private $q: ng.IQService;

        constructor($q: ng.IQService) {
            this.$q = $q;

            this.started = false;
            this.databases = {};
        }

        public start(): ng.IPromise<any> {
            var dfrd: ng.IDeferred<any> = this.$q.defer<any>();

            console.debug("DatabaseService.start()");

            var openRequest: IDBOpenDBRequest = indexedDB.open(DatabaseService.APP_DATABASE_NAME, DatabaseService.APP_DATABASE_VERSION);

            openRequest.onupgradeneeded = (ev: any)=>{

                var db: IDBDatabase = <IDBDatabase>ev.target.result;

                for (var i=0; i<DatabaseService.APP_DATABASE_SCHEMA.length; i++) {
                    var objectStoreSchema: ObjectStoreSchema = DatabaseService.APP_DATABASE_SCHEMA[i];

                    if(!db.objectStoreNames.contains(objectStoreSchema.name)) {
                        var objectStore: IDBObjectStore = db.createObjectStore(objectStoreSchema.name, { keyPath: objectStoreSchema.keyPath });

                        for (var t=0; t<objectStoreSchema.indexes.length; t++) {
                            var index: ObjectStoreIndex = objectStoreSchema.indexes[t];
                            objectStore.createIndex(index.name, index.keyPath, {unique: index.unique});
                        }
                    }
                }

                this.databases[DatabaseService.APP_DATABASE_NAME] = db;
            };

            openRequest.onsuccess = (ev: any)=>{
                this.databases[DatabaseService.APP_DATABASE_NAME] = <IDBDatabase>ev.target.result;
                this.started = true;
                dfrd.resolve();
            };

            openRequest.onerror = (ev: ErrorEvent)=>{
                dfrd.reject(ev);
            };

            return dfrd.promise;
        }

        public get(dbName: string): IDBDatabase {
            return this.databases[dbName];
        }

        public exportObjectStore(dbName: string, objectStoreName: string): ng.IPromise<Object[]> {
            var dfrd: ng.IDeferred<Object[]> = this.$q.defer<Object[]>();

            var db: IDBDatabase = this.databases[dbName];
            var transaction: IDBTransaction = db.transaction(objectStoreName, 'readonly');
            var store: IDBObjectStore = transaction.objectStore(objectStoreName);
            var request: IDBRequest = store.openCursor();
            var exportedDatabase = [];

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                var cursor: IDBCursor = e.target.result;
                if (cursor != null) {
                    // ugly cast to object to access the cursor value
                    exportedDatabase.push((<Object>cursor)["value"]);
                    cursor.continue();
                }
            };

            transaction.oncomplete = function(e: any) {
                dfrd.resolve(exportedDatabase);
            };

            return dfrd.promise;
        }

        public export(dbName: string): ng.IPromise<{[name: string]: Object[]}> {
            var dfrd: ng.IDeferred<{[name: string]: Object[]}> = this.$q.defer<{[name: string]: Object[]}>();

            var db: IDBDatabase = this.databases[dbName];
            var exportedDatabases: {[name: string]: Object[]} = {};

            var promises: ng.IPromise<Object[]>[] = [];
            for (var index = 0; index < db.objectStoreNames.length; index++) {
                var objectStoreName: string = db.objectStoreNames[index];
                promises.push(this.exportObjectStore(dbName, objectStoreName));
            }

            this.$q.all(
                promises
            ).then(
                (result: Object[][])=>{
                    for (var index = 0; index < db.objectStoreNames.length; index++) {
                        var objectStoreName: string = db.objectStoreNames[index];
                        exportedDatabases[objectStoreName] = result[index];
                    }
                }
            ).finally(
                ()=>{
                    dfrd.resolve(exportedDatabases);
                }
            );

            return dfrd.promise;
        }

        public clearObjectStore(dbName: string, objectStoreName: string): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var db: IDBDatabase = this.databases[dbName];
            var transaction: IDBTransaction = db.transaction(objectStoreName, 'readwrite');
            var store: IDBObjectStore = transaction.objectStore(objectStoreName);
            var request: IDBRequest = store.clear();

            request.onerror = function(e: any) {
                dfrd.reject(e);
            };

            request.onsuccess = function(e: any) {
                dfrd.resolve();
            };

            return dfrd.promise;
        }

        public clear(dbName: string): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var db: IDBDatabase = this.databases[dbName];

            var promises: ng.IPromise<void>[] = [];
            for (var index = 0; index < db.objectStoreNames.length; index++) {
                var objectStoreName: string = db.objectStoreNames[index];
                promises.push(this.clearObjectStore(dbName, objectStoreName));
            }

            this.$q.all(
                promises
            ).then(
                ()=>{
                    dfrd.resolve();
                }
            );

            return dfrd.promise;
        }

    }

}