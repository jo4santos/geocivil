module civilapp.model {

    export class Project {
        id: number;
        label: string;
        address: string;
        created: number;
        points: Point[];
    }

}