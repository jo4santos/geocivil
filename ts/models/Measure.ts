module civilapp.model {

    export class Measure {
        latitude: number;
        longitude: number;
        altitude: number;
    }

}