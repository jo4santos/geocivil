module civilapp.model {

    export class Point {
        id: number;
        label: string;
        coords: string;
        measure: Measure;
    }

}