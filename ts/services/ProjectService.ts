module civilapp.services {

    export class ProjectService {

        private $q:ng.IQService;

        private DatasourceFactory:civilapp.datasources.DatasourceFactory;
        private ProjectDatasource:civilapp.datasources.ProjectDatasource;

        private projects:model.Project[];

        static $inject = ['$q'];

        public constructor($q:ng.IQService, DatasourceFactory:civilapp.datasources.DatasourceFactory) {
            this.$q = $q;

            this.DatasourceFactory = DatasourceFactory;
            this.ProjectDatasource = this.DatasourceFactory.getProjectDatasource();

            this.projects = [];

            console.debug("ProjectService.constructor()");

            this.load();
        }

        private load():ng.IPromise<void> {
            var deferred:ng.IDeferred<void> = this.$q.defer<void>();
            this.projects = [];

            this.ProjectDatasource.retrieveAll()
                .then((projects:civilapp.model.Project[])=> {
                    for(var i in projects) {
                        if(!projects[i].points) projects[i].points = [];
                    }
                    this.projects = angular.copy(projects);
                    deferred.resolve();
                });
            return deferred.promise;
        }

        public create(label:string):ng.IPromise<string> {
            var deferred = this.$q.defer();

            var project:model.Project = new model.Project();

            project.id = 0;
            for (var i in this.projects) {
                if (this.projects[i].id > project.id) {
                    project.id = this.projects[i].id;
                }
            }

            project.id++;
            project.label = label;
            project.created = +new Date;

            this.ProjectDatasource.create(project)
                .then(()=> {
                    this.load()
                        .then(()=> {
                            this.getById(project.id)
                                .then((createdProject:model.Project)=> {
                                    deferred.resolve(createdProject.id);
                                });

                        })
                })
                .catch(() => {
                    deferred.reject();
                });

            return deferred.promise;
        }

        public getList():ng.IPromise<model.Project[]> {
            var deferred = this.$q.defer();

            this.load()
                .then(()=> {
                    deferred.resolve(this.projects);
                });

            return deferred.promise;
        }

        public getById(id:number):ng.IPromise<model.Project> {
            var deferred = this.$q.defer();
            this.load()
                .then(()=> {
                    for (var i in this.projects) {
                        if (this.projects[i].id == id) {
                            deferred.resolve(this.projects[i]);
                            return deferred.promise;
                        }
                    }
                    deferred.reject();
                })
            return deferred.promise;
        }

        public remove(id:number):ng.IPromise<model.Project> {
            var deferred = this.$q.defer();
            this.ProjectDatasource.removeById(id)
                .then(()=> {
                    deferred.resolve();
                })
                .catch(() => {
                    deferred.reject();
                });
            return deferred.promise;
        }

        public update(project:model.Project):ng.IPromise<string> {
            var deferred = this.$q.defer();

            this.ProjectDatasource.update(project)
                .then(()=> {
                    this.load().then(()=> {
                        deferred.resolve(project.id);
                    })
                })
                .catch(() => {
                    deferred.reject();
                });

            return deferred.promise;
        }
    }

}