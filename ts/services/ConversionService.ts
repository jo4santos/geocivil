module civilapp.services {

    export class ConversionService {

        private $q:ng.IQService;
        private $http:ng.IHttpService;

        static INPUT_TYPE = "EPSG:4326";
        static OUTPUT_TYPE = "+proj=tmerc +lat_0=39.66666666666666 +lon_0=-8.131906111111112 +k=1 +x_0=180.598 +y_0=-86.98999999999999 +ellps=intl +units=m +no_defs";

        public constructor($q:ng.IQService) {
            this.$q = $q;

            console.debug("ConversionService.constructor()");
        }

        public convert(latitude: number, longitude: number): number[] {
            return proj4(ConversionService.INPUT_TYPE,ConversionService.OUTPUT_TYPE,[latitude,longitude]);
        }
    }

}