module civilapp.services {

    export class ElevationService {

        private $q:ng.IQService;
        private $http:ng.IHttpService;

        static $inject = ['$q'];

        //static APIURL = "http://www.earthtools.org/height/{latitude}/{longitude}";
        static APIURL = "http://api.geonames.org/srtm3JSON?lat={latitude}&lng={longitude}&username=demo";

        public constructor($q:ng.IQService,$http:ng.IHttpService) {
            this.$q = $q;
            this.$http = $http;

            console.debug("ElevationService.constructor()");
        }

        public get(latitude: number, longitude: number): ng.IPromise<number> {
            var dfrd: ng.IDeferred<number> = this.$q.defer<number>();
            this.$http.get(ElevationService.APIURL.replace("{latitude}",latitude.toString()).replace("{longitude}",longitude.toString()))
                .then((data: any)=>{
                    dfrd.resolve(data.data.srtm3);
                });
            return dfrd.promise;
        }
    }

}