module civilapp.services {

    export class ExportService {

        private $cordovaFile: ng.cordova.IFileService;
        private $cordovaSocialSharing: ng.cordova.ISocialSharingService;
        private $cordovaToast: ng.cordova.IToastService;

        private $q:ng.IQService;

        private ConversionService: ConversionService;

        public constructor(
            $q:ng.IQService,
            $cordovaFile: ng.cordova.IFileService,
            $cordovaSocialSharing: ng.cordova.ISocialSharingService,
            $cordovaToast: ng.cordova.IToastService,
            ConversionService: ConversionService
            ) {

            this.$cordovaFile = $cordovaFile;
            this.$cordovaSocialSharing = $cordovaSocialSharing;
            this.$cordovaToast = $cordovaToast;
            this.$q = $q;
            this.ConversionService = ConversionService;

            console.debug("ExportService.constructor()");
        }

        public send (project: model.Project): ng.IPromise<void> {
            var dfrd: ng.IDeferred<void> = this.$q.defer<void>();

            var content: string = this.buildFileContent(project);
            var filename: string = this.buildFilename(project);

            this.$cordovaFile.writeFile(filename, content, true)
                .then((data) => {
                    console.log(data);
                    this.$cordovaSocialSharing
                        .share(null, null, cordova.file.externalRootDirectory+filename, null)
                        .then(() => {
                            dfrd.resolve();
                            this.$cordovaToast.showShortBottom('Ficheiro enviado');
                        })
                        .catch(() => {
                            dfrd.reject();
                            this.$cordovaToast.showShortBottom('Erro ao enviar ficheiro');
                        });

                }).catch((err) => {
                    console.error(err);
                    this.$cordovaToast.showShortBottom('Erro ao gerar ficheiro de coordenadas');
                    dfrd.reject()
                });

            return dfrd.promise;
        }

        private buildFileContent (project: model.Project): string {
            var content: string = "";

            for(var i in project.points) {
                var coords = this.ConversionService.convert(project.points[i].measure.latitude,project.points[i].measure.longitude);
                if(content!="") content += "\n";
                content += "Point "+coords[0]+","+coords[1]+","+project.points[i].measure.altitude;
            }

            return content;
        }

        private buildEmailContent (project: model.Project): string {
            var content: string = "";

            content += "Enviado a partir da CivilApp:";

            content += "\n\nProjecto";
            content += "\n_Nome: " + project.label;
            content += "\n_Data criação: "+project.created;
            if(project.address) {
                content += "\n_Morada: "+project.address;
            }
            content += "\n\nPontos";
            for(var i in project.points) {
                content += "\n\n_Nome: "+project.points[i].label;
                content += "\n_Latitude: "+project.points[i].measure.latitude;
                content += "\n_Longitude: "+project.points[i].measure.longitude;
                if(project.points[i].measure.altitude) {
                    content += "\n_Altitude: "+project.points[i].measure.altitude + "m";
                }
                content += "\n_Mapa: https://maps.googleapis.com/maps/api/staticmap?size=200x200&markers=color:red|label:" + project.points[i].id + "|" + project.points[i].measure.latitude + "," + project.points[i].measure.longitude + "&scale=2&zoom=18&sensor=true ";
            }

            return content;
        }

        private buildFilename (project: model.Project): string {
            var filename: string = "";

            filename += project.label + ".scr";

            return filename;
        }
    }

}