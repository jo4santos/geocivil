declare var cordova: any;

declare module ng.cordova {
    export interface IFileService {
        writeFile(name:string, content:string, overwrite:boolean): ng.IPromise<any>;
    }

    export interface ISocialSharingService {
        share(message:string, subject: string, file: string, link: string): ng.IPromise<any>;
    }
    export interface IToastService {
        showShortBottom(message:string): ng.IPromise<any>;
    }
}