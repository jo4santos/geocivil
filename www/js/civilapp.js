/// <reference path="utils/LogUtils.ts"/>
var civilapp;
(function (civilapp) {
    var civilApp = (function () {
        function civilApp() {
            this.bootstrapAngular();
        }
        civilApp.prototype.bootstrapAngular = function () {
            this.angularApp = angular.module(civilApp.ANGULAR_APP_NAME, [
                'ionic',
                'ngCordova'
            ]);
            this.angularApp.provider("DatabaseService", function () {
                var DatabaseService;
                this.$get = function ($q) {
                    if (DatabaseService == null) {
                        DatabaseService = new civilapp.services.DatabaseService($q);
                    }
                    return DatabaseService;
                };
            });
            this.angularApp.provider("DatasourceFactory", function () {
                var DatasourceFactory;
                this.$get = function (DatabaseService, $q) {
                    if (DatasourceFactory == null) {
                        DatasourceFactory = new civilapp.datasources.DatasourceFactory($q, DatabaseService);
                    }
                    return DatasourceFactory;
                };
            });
            this.angularApp.provider("ProjectService", function () {
                var ProjectService;
                this.$get = function ($q, DatasourceFactory) {
                    if (ProjectService == null) {
                        ProjectService = new civilapp.services.ProjectService($q, DatasourceFactory);
                    }
                    return ProjectService;
                };
            });
            this.angularApp.provider("ElevationService", function () {
                var ElevationService;
                this.$get = function ($q, $http) {
                    if (ElevationService == null) {
                        ElevationService = new civilapp.services.ElevationService($q, $http);
                    }
                    return ElevationService;
                };
            });
            this.angularApp.provider("ConversionService", function () {
                var ConversionService;
                this.$get = function ($q) {
                    if (ConversionService == null) {
                        ConversionService = new civilapp.services.ConversionService($q);
                    }
                    return ConversionService;
                };
            });
            this.angularApp.provider("ExportService", function () {
                var ExportService;
                this.$get = function ($q, $cordovaFile, $cordovaSocialSharing, $cordovaToast, ConversionService) {
                    if (ExportService == null) {
                        ExportService = new civilapp.services.ExportService($q, $cordovaFile, $cordovaSocialSharing, $cordovaToast, ConversionService);
                    }
                    return ExportService;
                };
            });
            this.angularApp.config(function ($urlRouterProvider, $stateProvider, $controllerProvider) {
                $stateProvider.state('init', {
                    url: '/init',
                    cache: false,
                    templateUrl: 'templates/init.html'
                }).state('project-list', {
                    url: '/',
                    cache: false,
                    templateUrl: 'templates/project-list.html',
                    controller: 'ProjectListController as ProjectListController'
                }).state('project', {
                    url: '/project/:projectId',
                    cache: false,
                    templateUrl: 'templates/project.html',
                    controller: 'ProjectController as ProjectController'
                }).state('point', {
                    url: '/point/:projectId/:pointId',
                    cache: false,
                    templateUrl: 'templates/point.html',
                    controller: 'PointController as PointController'
                });
                $urlRouterProvider.otherwise("/init");
                $controllerProvider.register("ProjectListController", civilapp.controllers.ProjectListController);
                $controllerProvider.register("ProjectController", civilapp.controllers.ProjectController);
                $controllerProvider.register("PointController", civilapp.controllers.PointController);
            });
            this.angularApp.run(function ($rootScope, $state, DatabaseService, $ionicHistory, $ionicPlatform) {
                $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                    if (!DatabaseService.started && toState.name != "init") {
                        event.preventDefault();
                        $state.go("init");
                    }
                    else if (toState.name == "init") {
                        DatabaseService.start().then(function () {
                            $ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            $state.go("project-list");
                        });
                    }
                });
            });
        };
        civilApp.ANGULAR_APP_NAME = "civilapp";
        return civilApp;
    })();
    civilapp.civilApp = civilApp;
})(civilapp || (civilapp = {}));
new civilapp.civilApp();
console.debug("[" + new Date().toUTCString() + "] - civilApp has just started!");

"use strict";
var civilapp;
(function (civilapp) {
    var controllers;
    (function (controllers) {
        var PointController = (function () {
            function PointController($scope, $state, $stateParams, ElevationService, ProjectService, $cordovaGeolocation, $ionicLoading) {
                this.$cordovaGeolocation = $cordovaGeolocation;
                this.$ionicLoading = $ionicLoading;
                this.$scope = $scope;
                this.$state = $state;
                this.$stateParams = $stateParams;
                this.project = new civilapp.model.Project();
                this.point = new civilapp.model.Point();
                this.project.id = parseInt(this.$stateParams.projectId);
                this.point.id = parseInt(this.$stateParams.pointId);
                this.ElevationService = ElevationService;
                this.ProjectService = ProjectService;
                if (!this.point.id) {
                    this.title = " - Novo ponto";
                }
                this.load();
                console.debug(civilapp.utils.LogUtils.format("PointController initialized"));
            }
            PointController.prototype.load = function () {
                var _this = this;
                this.ProjectService.getById(this.project.id).then(function (project) {
                    _this.project = angular.copy(project);
                    for (var i in _this.project.points) {
                        if (_this.project.points[i].id == _this.point.id) {
                            _this.point = angular.copy(_this.project.points[i]);
                            break;
                        }
                    }
                    if (!_this.point.id) {
                        _this.point.id = 0;
                        for (var i in _this.project.points) {
                            if (_this.project.points[i].id > _this.point.id) {
                                _this.point.id = _this.project.points[i].id;
                            }
                        }
                        _this.point.id++;
                        if (!_this.point.label || _this.point.label == "") {
                            _this.point.label = "Ponto " + _this.point.id;
                        }
                        _this.project.points.push(_this.point);
                    }
                    _this.title = " - " + _this.point.label;
                });
            };
            PointController.prototype.save = function () {
                if (!this.point.id) {
                }
                else {
                    if (!this.point.label || this.point.label == "") {
                        this.point.label = "Ponto " + this.point.id;
                    }
                    for (var i in this.project.points) {
                        if (this.project.points[i].id == this.point.id) {
                            this.project.points[i] = angular.copy(this.point);
                            break;
                        }
                    }
                }
                this.ProjectService.update(this.project);
            };
            PointController.prototype.register = function () {
                var _this = this;
                this.point.measure = this.point.measure || new civilapp.model.Measure();
                this.point.measure.altitude = null;
                this.$ionicLoading.show({ template: 'Loading...' });
                this.$cordovaGeolocation.getCurrentPosition({ timeout: 10000, enableHighAccuracy: true }).then(function (position) {
                    _this.point.measure.latitude = position.coords.latitude;
                    _this.point.measure.longitude = position.coords.longitude;
                    _this.$ionicLoading.hide();
                    _this.save();
                }).catch(function (err) {
                    console.log(err);
                    alert("ERROR");
                    _this.$ionicLoading.hide();
                    // error
                });
                this.save();
            };
            PointController.prototype.enhance = function () {
                var _this = this;
                this.point.measure = this.point.measure || new civilapp.model.Measure();
                this.ElevationService.get(this.point.measure.latitude, this.point.measure.longitude).then(function (data) {
                    _this.point.measure.altitude = angular.copy(data);
                    _this.save();
                });
            };
            return PointController;
        })();
        controllers.PointController = PointController;
    })(controllers = civilapp.controllers || (civilapp.controllers = {}));
})(civilapp || (civilapp = {}));

"use strict";
var civilapp;
(function (civilapp) {
    var controllers;
    (function (controllers) {
        var ProjectController = (function () {
            function ProjectController($scope, $ionicActionSheet, $ionicPopup, $state, $stateParams, ProjectService, ElevationService, ExportService) {
                this.$ionicActionSheet = $ionicActionSheet;
                this.$ionicPopup = $ionicPopup;
                this.$scope = $scope;
                this.$state = $state;
                this.$stateParams = $stateParams;
                this.ElevationService = ElevationService;
                this.ExportService = ExportService;
                this.ProjectService = ProjectService;
                this.load();
                console.debug(civilapp.utils.LogUtils.format("ProjectController initialized"));
            }
            ProjectController.prototype.load = function () {
                var _this = this;
                this.ProjectService.getById(parseInt(this.$stateParams.projectId)).then(function (project) {
                    _this.project = angular.copy(project);
                });
            };
            ProjectController.prototype.select = function (point) {
                var _this = this;
                var options = {};
                options.titleText = point.label;
                options.buttons = [
                    { text: "Abrir" },
                    { text: "Obter altitude" }
                ];
                options.buttonClicked = function (index) {
                    switch (index) {
                        case 0:
                            _this.$state.go("point", { projectId: _this.project.id, pointId: point.id });
                            hideSheet();
                            break;
                        case 1:
                            _this.enhance(point);
                            hideSheet();
                            break;
                    }
                };
                options.destructiveText = "Apagar";
                options.destructiveButtonClicked = function () {
                    _this.$ionicPopup.confirm({
                        title: 'Confirmar eliminação',
                        template: 'O ponto será removido e não poderá voltar atrás'
                    }).then(function (res) {
                        if (res) {
                            for (var i in _this.project.points) {
                                if (_this.project.points[i].id == point.id) {
                                    _this.project.points.splice(i, 1);
                                    break;
                                }
                            }
                            _this.ProjectService.update(_this.project).then(function () {
                                _this.load();
                                hideSheet();
                            });
                        }
                    });
                    return true;
                };
                options.cancelText = "Cancelar";
                options.cancel = function () {
                };
                var hideSheet = this.$ionicActionSheet.show(options);
            };
            ProjectController.prototype.share = function (project) {
                this.ExportService.send(project);
            };
            ProjectController.prototype.enhance = function (point) {
                var _this = this;
                point.measure = point.measure || new civilapp.model.Measure();
                this.ElevationService.get(point.measure.latitude, point.measure.longitude).then(function (data) {
                    point.measure.altitude = angular.copy(data);
                    _this.save();
                });
            };
            ProjectController.prototype.save = function () {
                var _this = this;
                this.ProjectService.update(this.project).then(function () {
                    _this.load();
                });
            };
            ProjectController.prototype.remove = function () {
                var _this = this;
                this.$ionicPopup.confirm({
                    title: 'Confirmar eliminação',
                    template: 'O projeto será removido e não poderá voltar atrás'
                }).then(function (res) {
                    if (res) {
                        _this.ProjectService.remove(_this.project.id).then(function () {
                            _this.$state.go("project-list");
                        });
                    }
                });
                return true;
            };
            ProjectController.prototype.getMapUrl = function () {
                var mapUrl = "https://maps.googleapis.com/maps/api/staticmap?size=200x200&";
                for (var i in this.project.points) {
                    mapUrl += "markers=color:red|label:" + this.project.points[i].id + "|" + this.project.points[i].measure.latitude + "," + this.project.points[i].measure.longitude + "&";
                }
                mapUrl += "scale=2&sensor=true";
                return mapUrl;
            };
            return ProjectController;
        })();
        controllers.ProjectController = ProjectController;
    })(controllers = civilapp.controllers || (civilapp.controllers = {}));
})(civilapp || (civilapp = {}));

"use strict";
var civilapp;
(function (civilapp) {
    var controllers;
    (function (controllers) {
        var ProjectListController = (function () {
            function ProjectListController($scope, ProjectService, $ionicActionSheet, $ionicPopup, $state) {
                this.$ionicActionSheet = $ionicActionSheet;
                this.$ionicPopup = $ionicPopup;
                this.$scope = $scope;
                this.$state = $state;
                this.ProjectService = ProjectService;
                this.projects = [];
                this.load();
                console.debug(civilapp.utils.LogUtils.format("ProjectListController initialized"));
            }
            ProjectListController.prototype.load = function () {
                var _this = this;
                this.ProjectService.getList().then(function (projects) {
                    _this.projects = angular.copy(projects);
                });
            };
            ProjectListController.prototype.create = function () {
                var _this = this;
                this.$ionicPopup.prompt({
                    title: "Insira um nome para o projeto",
                    inputPlaceholder: "nome",
                    inputType: 'text'
                }).then(function (projectLabel) {
                    if (projectLabel) {
                        _this.ProjectService.create(projectLabel).then(function (projectId) {
                            _this.load();
                            _this.$state.go("project", { projectId: projectId });
                        }).catch(function () {
                            alert("Erro ao criar");
                        });
                    }
                });
            };
            ProjectListController.prototype.select = function (project) {
                var _this = this;
                var options = {};
                options.titleText = project.label;
                options.buttons = [
                    { text: "Abrir" },
                    { text: "Partilhar" }
                ];
                options.buttonClicked = function (index) {
                    switch (index) {
                        case 0:
                            _this.$state.go("project", { projectId: project.id });
                            break;
                        case 1:
                            _this.share(project);
                            break;
                    }
                };
                options.destructiveText = "Apagar";
                options.destructiveButtonClicked = function () {
                    _this.$ionicPopup.confirm({
                        title: 'Confirmar eliminação',
                        template: 'O projeto será removido e não poderá voltar atrás'
                    }).then(function (res) {
                        if (res) {
                            _this.ProjectService.remove(project.id).then(function () {
                                _this.load();
                                hideSheet();
                            });
                        }
                    });
                    return true;
                };
                options.cancelText = "Cancelar";
                options.cancel = function () {
                };
                var hideSheet = this.$ionicActionSheet.show(options);
            };
            ProjectListController.prototype.share = function (project) {
                alert("share " + project.id);
            };
            return ProjectListController;
        })();
        controllers.ProjectListController = ProjectListController;
    })(controllers = civilapp.controllers || (civilapp.controllers = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var services;
    (function (services) {
        var DatabaseService = (function () {
            function DatabaseService($q) {
                this.$q = $q;
                this.started = false;
                this.databases = {};
            }
            DatabaseService.prototype.start = function () {
                var _this = this;
                var dfrd = this.$q.defer();
                console.debug("DatabaseService.start()");
                var openRequest = indexedDB.open(DatabaseService.APP_DATABASE_NAME, DatabaseService.APP_DATABASE_VERSION);
                openRequest.onupgradeneeded = function (ev) {
                    var db = ev.target.result;
                    for (var i = 0; i < DatabaseService.APP_DATABASE_SCHEMA.length; i++) {
                        var objectStoreSchema = DatabaseService.APP_DATABASE_SCHEMA[i];
                        if (!db.objectStoreNames.contains(objectStoreSchema.name)) {
                            var objectStore = db.createObjectStore(objectStoreSchema.name, { keyPath: objectStoreSchema.keyPath });
                            for (var t = 0; t < objectStoreSchema.indexes.length; t++) {
                                var index = objectStoreSchema.indexes[t];
                                objectStore.createIndex(index.name, index.keyPath, { unique: index.unique });
                            }
                        }
                    }
                    _this.databases[DatabaseService.APP_DATABASE_NAME] = db;
                };
                openRequest.onsuccess = function (ev) {
                    _this.databases[DatabaseService.APP_DATABASE_NAME] = ev.target.result;
                    _this.started = true;
                    dfrd.resolve();
                };
                openRequest.onerror = function (ev) {
                    dfrd.reject(ev);
                };
                return dfrd.promise;
            };
            DatabaseService.prototype.get = function (dbName) {
                return this.databases[dbName];
            };
            DatabaseService.prototype.exportObjectStore = function (dbName, objectStoreName) {
                var dfrd = this.$q.defer();
                var db = this.databases[dbName];
                var transaction = db.transaction(objectStoreName, 'readonly');
                var store = transaction.objectStore(objectStoreName);
                var request = store.openCursor();
                var exportedDatabase = [];
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    var cursor = e.target.result;
                    if (cursor != null) {
                        // ugly cast to object to access the cursor value
                        exportedDatabase.push(cursor["value"]);
                        cursor.continue();
                    }
                };
                transaction.oncomplete = function (e) {
                    dfrd.resolve(exportedDatabase);
                };
                return dfrd.promise;
            };
            DatabaseService.prototype.export = function (dbName) {
                var dfrd = this.$q.defer();
                var db = this.databases[dbName];
                var exportedDatabases = {};
                var promises = [];
                for (var index = 0; index < db.objectStoreNames.length; index++) {
                    var objectStoreName = db.objectStoreNames[index];
                    promises.push(this.exportObjectStore(dbName, objectStoreName));
                }
                this.$q.all(promises).then(function (result) {
                    for (var index = 0; index < db.objectStoreNames.length; index++) {
                        var objectStoreName = db.objectStoreNames[index];
                        exportedDatabases[objectStoreName] = result[index];
                    }
                }).finally(function () {
                    dfrd.resolve(exportedDatabases);
                });
                return dfrd.promise;
            };
            DatabaseService.prototype.clearObjectStore = function (dbName, objectStoreName) {
                var dfrd = this.$q.defer();
                var db = this.databases[dbName];
                var transaction = db.transaction(objectStoreName, 'readwrite');
                var store = transaction.objectStore(objectStoreName);
                var request = store.clear();
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    dfrd.resolve();
                };
                return dfrd.promise;
            };
            DatabaseService.prototype.clear = function (dbName) {
                var dfrd = this.$q.defer();
                var db = this.databases[dbName];
                var promises = [];
                for (var index = 0; index < db.objectStoreNames.length; index++) {
                    var objectStoreName = db.objectStoreNames[index];
                    promises.push(this.clearObjectStore(dbName, objectStoreName));
                }
                this.$q.all(promises).then(function () {
                    dfrd.resolve();
                });
                return dfrd.promise;
            };
            DatabaseService.APP_DATABASE_VERSION = 1;
            DatabaseService.APP_DATABASE_NAME = "AppDatabase";
            DatabaseService.APP_DATABASE_SCHEMA = [
                {
                    name: "Project",
                    keyPath: "id",
                    indexes: []
                }
            ];
            return DatabaseService;
        })();
        services.DatabaseService = DatabaseService;
    })(services = civilapp.services || (civilapp.services = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var datasources;
    (function (datasources) {
        var DatasourceFactory = (function () {
            function DatasourceFactory($q, DatabaseService) {
                this.DatabaseService = DatabaseService;
                console.debug("DatasourceFactory.constructor()");
                this.$q = $q;
            }
            DatasourceFactory.prototype.getProjectDatasource = function () {
                return new datasources.ProjectDatasource(this.DatabaseService, this.$q);
            };
            return DatasourceFactory;
        })();
        datasources.DatasourceFactory = DatasourceFactory;
    })(datasources = civilapp.datasources || (civilapp.datasources = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var datasources;
    (function (datasources) {
        var ProjectDatasource = (function () {
            function ProjectDatasource(DatabaseService, $q) {
                this.$q = $q;
                console.log("ProjectDatasource.constructor()");
                this.db = DatabaseService.get(civilapp.services.DatabaseService.APP_DATABASE_NAME);
            }
            ProjectDatasource.prototype.create = function (project) {
                var dfrd = this.$q.defer();
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.add(project);
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    dfrd.resolve();
                };
                return dfrd.promise;
            };
            ProjectDatasource.prototype.retrieveAll = function () {
                var dfrd = this.$q.defer();
                var projects = [];
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readonly");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.openCursor();
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    var result = e.target.result;
                    if (result) {
                        projects.push(result.value);
                        result.continue();
                    }
                    else {
                        dfrd.resolve(projects);
                    }
                };
                return dfrd.promise;
            };
            ProjectDatasource.prototype.retrieveById = function (id) {
                var dfrd = this.$q.defer();
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readonly");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.get(id);
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    var result = e.target.result;
                    if (result != null) {
                        var project = new civilapp.model.Project();
                        angular.copy(result, project);
                        dfrd.resolve(project);
                    }
                    else {
                        dfrd.resolve(null);
                    }
                };
                return dfrd.promise;
            };
            ProjectDatasource.prototype.update = function (project) {
                var dfrd = this.$q.defer();
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.put(project);
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    dfrd.resolve();
                };
                return dfrd.promise;
            };
            ProjectDatasource.prototype.removeById = function (id) {
                var dfrd = this.$q.defer();
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.delete(id);
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    dfrd.resolve();
                };
                return dfrd.promise;
            };
            ProjectDatasource.prototype.clear = function () {
                var dfrd = this.$q.defer();
                var transaction = this.db.transaction([ProjectDatasource.TABLE_NAME], "readwrite");
                var store = transaction.objectStore(ProjectDatasource.TABLE_NAME);
                var request = store.clear();
                request.onerror = function (e) {
                    dfrd.reject(e);
                };
                request.onsuccess = function (e) {
                    dfrd.resolve();
                };
                return dfrd.promise;
            };
            ProjectDatasource.TABLE_NAME = "Project";
            return ProjectDatasource;
        })();
        datasources.ProjectDatasource = ProjectDatasource;
    })(datasources = civilapp.datasources || (civilapp.datasources = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var model;
    (function (model) {
        var Measure = (function () {
            function Measure() {
            }
            return Measure;
        })();
        model.Measure = Measure;
    })(model = civilapp.model || (civilapp.model = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var model;
    (function (model) {
        var Point = (function () {
            function Point() {
            }
            return Point;
        })();
        model.Point = Point;
    })(model = civilapp.model || (civilapp.model = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var model;
    (function (model) {
        var Project = (function () {
            function Project() {
            }
            return Project;
        })();
        model.Project = Project;
    })(model = civilapp.model || (civilapp.model = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var services;
    (function (services) {
        var ConversionService = (function () {
            function ConversionService($q) {
                this.$q = $q;
                console.debug("ConversionService.constructor()");
            }
            ConversionService.prototype.convert = function (latitude, longitude) {
                return proj4(ConversionService.INPUT_TYPE, ConversionService.OUTPUT_TYPE, [latitude, longitude]);
            };
            ConversionService.INPUT_TYPE = "EPSG:4326";
            ConversionService.OUTPUT_TYPE = "+proj=tmerc +lat_0=39.66666666666666 +lon_0=-8.131906111111112 +k=1 +x_0=180.598 +y_0=-86.98999999999999 +ellps=intl +units=m +no_defs";
            return ConversionService;
        })();
        services.ConversionService = ConversionService;
    })(services = civilapp.services || (civilapp.services = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var services;
    (function (services) {
        var ElevationService = (function () {
            function ElevationService($q, $http) {
                this.$q = $q;
                this.$http = $http;
                console.debug("ElevationService.constructor()");
            }
            ElevationService.prototype.get = function (latitude, longitude) {
                var dfrd = this.$q.defer();
                this.$http.get(ElevationService.APIURL.replace("{latitude}", latitude.toString()).replace("{longitude}", longitude.toString())).then(function (data) {
                    dfrd.resolve(data.data.srtm3);
                });
                return dfrd.promise;
            };
            ElevationService.$inject = ['$q'];
            //static APIURL = "http://www.earthtools.org/height/{latitude}/{longitude}";
            ElevationService.APIURL = "http://api.geonames.org/srtm3JSON?lat={latitude}&lng={longitude}&username=demo";
            return ElevationService;
        })();
        services.ElevationService = ElevationService;
    })(services = civilapp.services || (civilapp.services = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var services;
    (function (services) {
        var ExportService = (function () {
            function ExportService($q, $cordovaFile, $cordovaSocialSharing, $cordovaToast, ConversionService) {
                this.$cordovaFile = $cordovaFile;
                this.$cordovaSocialSharing = $cordovaSocialSharing;
                this.$cordovaToast = $cordovaToast;
                this.$q = $q;
                this.ConversionService = ConversionService;
                console.debug("ExportService.constructor()");
            }
            ExportService.prototype.send = function (project) {
                var _this = this;
                var dfrd = this.$q.defer();
                var content = this.buildFileContent(project);
                var filename = this.buildFilename(project);
                this.$cordovaFile.writeFile(filename, content, true).then(function (data) {
                    console.log(data);
                    _this.$cordovaSocialSharing.share(null, null, cordova.file.externalRootDirectory + filename, null).then(function () {
                        dfrd.resolve();
                        _this.$cordovaToast.showShortBottom('Ficheiro enviado');
                    }).catch(function () {
                        dfrd.reject();
                        _this.$cordovaToast.showShortBottom('Erro ao enviar ficheiro');
                    });
                }).catch(function (err) {
                    console.error(err);
                    _this.$cordovaToast.showShortBottom('Erro ao gerar ficheiro de coordenadas');
                    dfrd.reject();
                });
                return dfrd.promise;
            };
            ExportService.prototype.buildFileContent = function (project) {
                var content = "";
                for (var i in project.points) {
                    var coords = this.ConversionService.convert(project.points[i].measure.latitude, project.points[i].measure.longitude);
                    if (content != "")
                        content += "\n";
                    content += "Point " + coords[0] + "," + coords[1] + "," + project.points[i].measure.altitude;
                }
                return content;
            };
            ExportService.prototype.buildEmailContent = function (project) {
                var content = "";
                content += "Enviado a partir da CivilApp:";
                content += "\n\nProjecto";
                content += "\n_Nome: " + project.label;
                content += "\n_Data criação: " + project.created;
                if (project.address) {
                    content += "\n_Morada: " + project.address;
                }
                content += "\n\nPontos";
                for (var i in project.points) {
                    content += "\n\n_Nome: " + project.points[i].label;
                    content += "\n_Latitude: " + project.points[i].measure.latitude;
                    content += "\n_Longitude: " + project.points[i].measure.longitude;
                    if (project.points[i].measure.altitude) {
                        content += "\n_Altitude: " + project.points[i].measure.altitude + "m";
                    }
                    content += "\n_Mapa: https://maps.googleapis.com/maps/api/staticmap?size=200x200&markers=color:red|label:" + project.points[i].id + "|" + project.points[i].measure.latitude + "," + project.points[i].measure.longitude + "&scale=2&zoom=18&sensor=true ";
                }
                return content;
            };
            ExportService.prototype.buildFilename = function (project) {
                var filename = "";
                filename += project.label + ".scr";
                return filename;
            };
            return ExportService;
        })();
        services.ExportService = ExportService;
    })(services = civilapp.services || (civilapp.services = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var services;
    (function (services) {
        var ProjectService = (function () {
            function ProjectService($q, DatasourceFactory) {
                this.$q = $q;
                this.DatasourceFactory = DatasourceFactory;
                this.ProjectDatasource = this.DatasourceFactory.getProjectDatasource();
                this.projects = [];
                console.debug("ProjectService.constructor()");
                this.load();
            }
            ProjectService.prototype.load = function () {
                var _this = this;
                var deferred = this.$q.defer();
                this.projects = [];
                this.ProjectDatasource.retrieveAll().then(function (projects) {
                    for (var i in projects) {
                        if (!projects[i].points)
                            projects[i].points = [];
                    }
                    _this.projects = angular.copy(projects);
                    deferred.resolve();
                });
                return deferred.promise;
            };
            ProjectService.prototype.create = function (label) {
                var _this = this;
                var deferred = this.$q.defer();
                var project = new civilapp.model.Project();
                project.id = 0;
                for (var i in this.projects) {
                    if (this.projects[i].id > project.id) {
                        project.id = this.projects[i].id;
                    }
                }
                project.id++;
                project.label = label;
                project.created = +new Date;
                this.ProjectDatasource.create(project).then(function () {
                    _this.load().then(function () {
                        _this.getById(project.id).then(function (createdProject) {
                            deferred.resolve(createdProject.id);
                        });
                    });
                }).catch(function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            ProjectService.prototype.getList = function () {
                var _this = this;
                var deferred = this.$q.defer();
                this.load().then(function () {
                    deferred.resolve(_this.projects);
                });
                return deferred.promise;
            };
            ProjectService.prototype.getById = function (id) {
                var _this = this;
                var deferred = this.$q.defer();
                this.load().then(function () {
                    for (var i in _this.projects) {
                        if (_this.projects[i].id == id) {
                            deferred.resolve(_this.projects[i]);
                            return deferred.promise;
                        }
                    }
                    deferred.reject();
                });
                return deferred.promise;
            };
            ProjectService.prototype.remove = function (id) {
                var deferred = this.$q.defer();
                this.ProjectDatasource.removeById(id).then(function () {
                    deferred.resolve();
                }).catch(function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            ProjectService.prototype.update = function (project) {
                var _this = this;
                var deferred = this.$q.defer();
                this.ProjectDatasource.update(project).then(function () {
                    _this.load().then(function () {
                        deferred.resolve(project.id);
                    });
                }).catch(function () {
                    deferred.reject();
                });
                return deferred.promise;
            };
            ProjectService.$inject = ['$q'];
            return ProjectService;
        })();
        services.ProjectService = ProjectService;
    })(services = civilapp.services || (civilapp.services = {}));
})(civilapp || (civilapp = {}));

var civilapp;
(function (civilapp) {
    var utils;
    (function (utils) {
        var LogUtils = (function () {
            function LogUtils() {
            }
            LogUtils.format = function (data) {
                return "[" + new Date().toUTCString() + "] - " + data;
            };
            return LogUtils;
        })();
        utils.LogUtils = LogUtils;
    })(utils = civilapp.utils || (civilapp.utils = {}));
})(civilapp || (civilapp = {}));
